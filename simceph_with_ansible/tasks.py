from enoslib.api import generate_inventory, run_ansible
from enoslib.task import enostask
from enoslib.infra.enos_g5k.provider import G5k
from enoslib.infra.enos_vagrant.provider import Enos_vagrant
import logging
import os

from simceph_with_ansible.constants import ANSIBLE_DIR

logger = logging.getLogger(__name__)


@enostask(new=True)
def g5k(config, force, env=None, **kwargs):
    provider = G5k(config["g5k"])
    roles, networks = provider.init(force_deploy=force)
    env["config"] = config
    env["roles"] = roles
    env["networks"] = networks


@enostask(new=True)
def vagrant(config, force, env=None, **kwargs):
    provider = Enos_vagrant(config["vagrant"])
    roles, networks = provider.init(force_deploy=force)
    env["config"] = config
    env["roles"] = roles
    env["networks"] = networks


@enostask(new=True)
def static(config, force, env=None, **kwargs):
    provider = Static(config["static"])
    roles, networks = provider.init(force_deploy=force)
    env["config"] = config
    env["roles"] = roles
    env["networks"] = networks


@enostask()
def inventory(**kwargs):
    env = kwargs["env"]
    roles = env["roles"]
    networks = env["networks"]
    env["inventory"] = os.path.join(env["resultdir"], "hosts")
    generate_inventory(roles, networks, env["inventory"], check_networks=True)
    # # adding path to group_vars file
    # if env["config"]["g5k"]["env_name"]=="ubuntu1804-x64-min":
    #     try:
    #         os.mkdir(os.path.join(env["resultdir"], "group_vars"))
    #     except:
    #         pass
    #     with open(os.path.join(env["resultdir"], "group_vars", "all.yml"),"w") as f:
    #         f.write("- ansible_python_interpreter: /usr/bin/python3")
    # # adding path to each host
    # for role in roles.keys():
    #     for d in roles[role]:
    #         d.extra["ansible_python_interpreter"]="/usr/bin/python3"



@enostask()
def prepare(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        "enos_action": "prepare_ceph"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)


@enostask()
def backup(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        "enos_action": "backup"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)


@enostask()
def destroy(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        "enos_action": "destroy"
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)


@enostask()
def compile_ceph(**kwargs):
    env = kwargs["env"]
    extra_vars = {
        "enos_action": "compile_ceph",
        "ceph_git_repo": kwargs.get("repo","https://github.com/Kayjukh/ceph.git"),
        "ceph_git_branch": kwargs.get("branch","working14.1.0")
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)


@enostask()
def run_ceph(**kwargs):
    from datetime import datetime
    env = kwargs["env"]
    extra_vars = {
            "enos_action": kwargs["which_bench"], # bench type (task found in $PROJECT_DIR/simceph-with-ansible/ansible/roles/compute/{{emos_action}}.yml)
        "default_params": kwargs["bench_opts"],
        "exp_params_list": [{} for e in range(kwargs["bench_opts"]["loops"])],
        "local_dir": env["resultdir"],
        "bench_id": "bench_" + datetime.today().isoformat()
        #{            # bench options
         #   "ceph_vstart": True,
          #  "ceph_create_bench_pool": True
           # }
    }
    run_ansible([os.path.join(ANSIBLE_DIR, "site.yml")],
                env["inventory"], extra_vars=extra_vars)


@enostask()
def plot(**kwargs):
    from datetime import datetime
    env = kwargs["env"]
    benchs= os.path.join(env["resultdir"],"fetched_exp_results", kwargs["which_bench"])
    print(benchs)
    benchs_id = os.listdir(benchs)
    print(benchs_id)
    for e in benchs_id:
        curr = os.listdir(os.path.join(benchs,e,"0"))
        print(curr)


PROVIDERS = {
    "g5k": g5k,
    "vagrant": vagrant,
    "static": static
    #    "chameleon": chameleon
}


