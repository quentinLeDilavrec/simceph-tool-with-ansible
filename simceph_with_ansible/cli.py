import faulthandler; faulthandler.enable()
import click
import logging
import yaml

import simceph_with_ansible.tasks as t
from simceph_with_ansible.constants import CONF

logging.basicConfig(level=logging.DEBUG)


@click.group()
def cli():
    pass


def load_config(file_path):
    """
    Read configuration from a file in YAML format.
    :param file_path: Path of the configuration file.
    :return:
    """
    with open(file_path) as f:
        configuration = yaml.safe_load(f)
    return configuration


@cli.command(help="Claim resources on Grid'5000 (frontend).")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def g5k(force, conf, env):
    config = load_config(conf)
    t.g5k(config, force, env=env)


@cli.command(help="Claim resources on vagrant (localhost).")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def vagrant(force, conf, env):
    config = load_config(conf)
    t.vagrant(config, force, env=env)


@cli.command(help="Generate the Ansible inventory [after g5k or vagrant].")
@click.option("--env",
              help="alternative environment directory")
def inventory(env):
    t.inventory(env=env)


@cli.command(help="Configure available resources [after deploy, inventory or\
             destroy].")
@click.option("--env",
              help="alternative environment directory")
def prepare(env):
    t.prepare(env=env)


@cli.command(help="Backup the deployed environment")
@click.option("--env",
              help="alternative environment directory")
def backup(env):
    t.backup(env=env)


@cli.command(help="Destroy the deployed environment")
@click.option("--env",
              help="alternative environment directory")
def destroy(env):
    t.destroy(env=env)


@cli.command(help="Claim resources from a PROVIDER and configure them.")
@click.argument("provider")
@click.option("--force",
              is_flag=True,
              help="force redeployment")
@click.option("--conf",
              default=CONF,
              help="alternative configuration file")
@click.option("--env",
              help="alternative environment directory")
def deploy(provider, force, conf, env):
    config = load_config(conf)
    t.PROVIDERS[provider](config, force, env=env)
    t.inventory()
    t.prepare(env=env)


@cli.command(help="Compile things")
@click.option("--ceph",
              help="compile ceph")
@click.option("--clang",
              is_flag=True,
              help="compile clang")

@click.option("--env",
              help="alternative environment directory")
def compile(ceph, clang, env):
    if ceph is not None and not clang:
        repo, branch = ceph.split(',',1)
        t.compile_ceph(env=env,repo=repo,branch=branch)
    elif clang:
        pass
        #t.compile_clang(env=env)


@cli.command(help="Run ceph cluster")
@click.option("--env",
              help="alternative environment directory")
@click.option("-b", "--which-bench",
              help="choose which bench to use")
def run_ceph(env,which_bench):
    t.run_ceph(env=env, which_bench=which_bench,bench_opts={"time":200,"loops":16,"step":3})

@cli.command(help="Plot results")
@click.option("--env",
              help="alternative environment directory")
@click.option("-b", "--which-bench",
              help="choose which bench to plot")
def plot(env,which_bench):
    t.plot(env=env, which_bench=which_bench)
