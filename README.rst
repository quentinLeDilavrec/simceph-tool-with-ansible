simceph with ansible 
=====================================
working on debian-9

localhost version is for later. (because install_deps.sh don't work on my system))

You can take inspiration from the ansible scripts to do it the standard bash way
or run ansible script directly with ansible
and even with vagrant by replacing ``swa deploy g5k`` by ``swa deploy vagrant``.

On g5k frontend

.. code:: bash

  cd
  python3 -m virtualenv env
  source env/bin/activate
  git clone https://gitlab.com/quentinLeDilavrec/simceph-tool-with-ansible.git
  cd simceph-tool-with-ansible
  pip install -e .
  swa deploy g5k
  swa compile --ceph